<div>
<form method="post" action="{{ url('inventory/'.$produks->id) }}">
@csrf
<input type="hidden" name="_method" value="PATCH">
                      <div >
                          <label>Nama Barang</label>
                          <input type="text"  name="nama_barang" placeholder="Isi Nama Barang" value="{{ $produks->nama_barang}}">
                      </div>
                      <div >
                          <label>Kode Barang</label>
                          <input type="text"  name="kode_barang" placeholder="Isi Kode Barang" value="{{ $produks->kode_barang}}">
                      </div>
                      <div >
                          <label>Jumlah Barang</label>
                          <input type="number" min="1"  name="jumlah_barang" placeholder="Isi Jumlah Barang" value="{{ $produks->jumlah_barang}}">
                      </div>
                      <div >
                          <label>Tanggal</label>
                          <input type="date"  name="tanggal"  value="{{ $produks->tanggal}}">
                      </div>
                   
                      <div>
                          <button>Simpan</button>
                      </div>
                  </form>
</div>