
<button><a href="{{ url('inventory/create') }}">Tambah</a></button>

<br>

<form method="GET" action="{{ url('inventory') }}">

<input type="test" name="cari" value="{{ $cari }}" placeholder="Cari......">
<button type="submit">Cari</button>
</form>

<br/>
<table>
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Barang</th>
                    <th>Kode Barang</th>
                    <th>Jumlah Barang</th>
                    <th>Tanggal</th>
                    <th>Aksi</th>

                </tr>
            </thead>
            <tbody>
            @foreach ($produks as $produk)
                <tr>
                    <th>{{ $loop->iteration }}</th>
                    <td>{{ $produk->nama_barang }}</td>
                    <td>{{ $produk->kode_barang}}</td>
                    <td>{{ $produk->jumlah_barang }}</td>
                    <td>{{ $produk->tanggal }}</td>
                    <td>
                        <button><a href="{{ url('inventory/'.$produk->id.'/edit') }}">Edit</a></button>
                        <form method="post" action="{{ url('inventory/'.$produk->id) }}">
                        @csrf
                        <input type="hidden" name="_method" value="Delete">
                        <button>Hapus</button>
                        </form>
                    </td>
                  
                </tr>
            @endforeach
            </tbody>
        </table>