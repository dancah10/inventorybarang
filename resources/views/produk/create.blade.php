<div>

<form method="post" action="{{ url('inventory') }}">
@csrf
                      <div >
                          <label>Nama Barang</label>
                          <input type="text"  name="nama_barang" placeholder="Isi Nama Barang" required>
                      </div>
                      <div >
                          <label>Kode Barang</label>
                          <input type="text"  name="kode_barang" placeholder="Isi Kode Barang" required>
                      </div>
                      <div >
                          <label>Jumlah Barang</label>
                          <input type="number" min="1"  name="jumlah_barang" placeholder="Isi Jumlah Barang" required>
                      </div>
                      <div >
                          <label>Tanggal</label>
                          <input type="date"  name="tanggal"  required>
                      </div>
                   
                      <div>
                          <button>Simpan</button>
                      </div>
                  </form>
</div>