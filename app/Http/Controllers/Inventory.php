<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Produk;
class Inventory extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $cari = $request->cari;

        $produks = Produk::where('nama_barang','like','%'.$cari.'%')
        ->orwhere('kode_barang','like','%'.$cari.'%')
        ->get();

        return view('produk.index',compact(
            'produks',
            'cari'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $produks = new Produk;

        return view('produk.create',compact(
            'produks'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $produks = new Produk;

        $produks->nama_barang = $request->nama_barang;
        $produks->kode_barang = $request->kode_barang;
        $produks->jumlah_barang = $request->jumlah_barang;
        $produks->tanggal = $request->tanggal;
        $produks->save();
        return redirect('inventory');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $produks = Produk::find($id);

        return view('produk.edit',compact(
            'produks'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $produks = Produk::find($id);

        $produks->nama_barang = $request->nama_barang;
        $produks->kode_barang = $request->kode_barang;
        $produks->jumlah_barang = $request->jumlah_barang;
        $produks->tanggal = $request->tanggal;
        $produks->save();
        return redirect('inventory');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produks = Produk::find($id);
        $produks->delete();
        return redirect('inventory');
    }
}
